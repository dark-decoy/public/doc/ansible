# Ansible General Sources List

* [Ansible Network Examples — Ansible Documentation](https://docs.ansible.com/ansible/latest/network/user_guide/network_best_practices_2.5.html "Ansible Network Examples — Ansible Documentation")
* [Implementing Ansible AWX — Gitlab integration \| by Marko Skender \| ITNEXT](https://itnext.io/implementing-ansible-awx-gitlab-integration-52c98f113ad0 "Implementing Ansible AWX — Gitlab integration | by Marko Skender | ITNEXT")
* [Mitogen — Mitogen Documentation](https://mitogen.networkgenomics.com/?_pk_id=&_pk_ses=&u=https%3A%2F%2Fnetworkgenomics.com%2Fansible%2F "Mitogen — Mitogen Documentation")
* [How to write a simple callback plugin for ansible \- Technekey\.com](https://technekey.com/how-to-write-a-simple-callback-plugin-for-ansible/ "How to write a simple callback plugin for ansible - Technekey.com")
* [ansible\/eda\-server\: Event Driven Ansible for AAP](https://github.com/ansible/eda-server "ansible/eda-server: Event Driven Ansible for AAP")
* [Controlling where tasks run\: delegation and local actions — Ansible Community Documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_delegation.html "Controlling where tasks run: delegation and local actions — Ansible Community Documentation")
* [Ansible Get Facts](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_vars_facts.html)
* [Ansible\, Windows and PowerShell\: the Basics – Part 5\, Example PowerShell Error Handling \- Jonathan Medd\'s Blog](https://www.jonathanmedd.net/2019/10/ansible-windows-and-powershell-the-basics-part-5-example-powershell-error-handling.html "Ansible, Windows and PowerShell: the Basics – Part 5, Example PowerShell Error Handling - Jonathan Medd\'s Blog")
* [Loops — Ansible Community Documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_loops.html#retrying-a-task-until-a-condition-is-met "Loops — Ansible Community Documentation")
* [Everything about Ansible Handlers](https://blog.learncodeonline.in/everything-about-ansible-handlers "Everything about Ansible Handlers")
