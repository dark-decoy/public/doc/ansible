# Jinja2 Sources List

* [Using filters to manipulate data — Ansible Community Documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_filters.html#handling-undefined-variables "Using filters to manipulate data — Ansible Community Documentation")
* [Mastering loops with Jinja templates in Ansible \| Enable Sysadmin](https://www.redhat.com/sysadmin/ansible-jinja "Mastering loops with Jinja templates in Ansible | Enable Sysadmin")
* [Template Designer Documentation — Jinja Documentation \(3\.0\.x\)](https://jinja.palletsprojects.com/en/3.0.x/templates/ "Template Designer Documentation — Jinja Documentation \(3.0.x\)")
* [Jinja2\: Check If Variable \- Empty \| Exists \| Defined \| True \- ShellHacks](https://www.shellhacks.com/jinja2-check-if-variable-empty-exists-defined-true/ "Jinja2: Check If Variable - Empty | Exists | Defined | True - ShellHacks")
